<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Acerca de</title>
	<base href="{{base_url}}" />
			<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
	<meta name="keywords" content="" />
		<meta name="generator" content="Zyro - Website Builder" />
	
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/main.js" type="text/javascript"></script>

	<link href="css/site.css?v=1.1.30" rel="stylesheet" type="text/css" />
	<link href="css/common.css?ts=1462483823" rel="stylesheet" type="text/css" />
	<link href="css/2.css?ts=1462483823" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="/gallery/wind_logo.png" type="image/png" />
	<script type="text/javascript">var currLang = '';</script>		
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>


<body>{{ga_code}}<div class="root"><div class="vbox wb_container" id="wb_header">
	
<div id="wb_element_instance28" class="wb_element"><a class="btn btn-default btn-collapser"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><ul class="hmenu"><li><a href="Inicio/" target="_self" title="Inicio">Inicio</a></li><li class="active"><a href="Acerca-de/" target="_self" title="Acerca de">Acerca de</a></li><li><a href="Como-Funciona/" target="_self" title="Como Funciona">Como Funciona</a></li><li><a href="Contactos/" target="_self" title="Contactos">Contactos</a></li></ul><script type="text/javascript"> (function() { var isOpen = false, elem = $('#wb_element_instance28'), btn = elem.children('.btn-collapser').eq(0); btn.on('click', function() { if (elem.hasClass('collapse-expanded')) { isOpen = false; elem.removeClass('collapse-expanded'); } else { isOpen = true; elem.addClass('collapse-expanded'); } }); })(); </script></div><div id="wb_element_instance29" class="wb_element" style=" line-height: normal;"><h4 class="wb-stl-pagetitle">Ecot - Center</h4>
</div><div id="wb_element_instance30" class="wb_element"><a href="Inicio/"><img alt="wind_logo" src="gallery_gen/0f3516698ba893423b36a442868afd23_90x90.png"></a></div></div>
<div class="vbox wb_container" id="wb_main">
	
<div id="wb_element_instance35" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1">Calentadores Solares</h1>
</div><div id="wb_element_instance36" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal" style="text-align: justify;">Es una empresa alteña dedicada especialmente a la venta, instalación y mantenimiento de paneles solares generadores de energía eléctrica para el sector industrial jalisciense, comprometida y responsable con el medio ambiente y en apoyo a la ecología global. </p>
</div><div id="wb_element_instance37" class="wb_element"><img alt="calentador" src="gallery_gen/19485d76fc54c99076c7bcf248b77e81_280x195.jpg"></div><div id="wb_element_instance38" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal" style="text-align: justify;"> * Responsabilidad​</p>

<p class="wb-stl-normal" style="text-align: justify;"> </p>

<p class="wb-stl-normal"> * Respeto</p>

<p class="wb-stl-normal"> </p>

<p class="wb-stl-normal"> * Honestidad </p>

<p class="wb-stl-normal"> </p>

<p class="wb-stl-normal"> * Calidad</p>
</div><div id="wb_element_instance39" class="wb_element"><img alt="wind_photo6" src="gallery_gen/179ce00ce96605d2f25c9e0e01e998ca_290x220.jpg"></div><div id="wb_element_instance40" class="wb_element"><img alt="paneles-solares-se-cargan-con-energía-solar" src="gallery_gen/baf828277f851907b2cb57d7228dbe5a_290x220.jpg"></div><div id="wb_element_instance41" class="wb_element" style=" line-height: normal;"><ul><li class="wb-stl-normal">Es ecológico ya que funciona con luz solar </li>
	<li class="wb-stl-normal">Tiene un ciclo de vida de 30 años ya que esta echo con materiales inoxidables.</li>
	<li class="wb-stl-normal">El gasto que se realiza se paga por si solo en aproximadamente 2 años</li>
	<li class="wb-stl-normal">
	<p>Se puede tener mayor iluminación sin incrementar los gastos</p>
	</li>
</ul></div><div id="wb_element_instance42" class="wb_element" style=" line-height: normal;"><h2 class="wb-stl-heading2">Nuestra misión</h2>
</div><div id="wb_element_instance43" class="wb_element" style=" line-height: normal;"><h2 class="wb-stl-heading2">Nuestros Valores</h2>
</div><div id="wb_element_instance44" class="wb_element" style=" line-height: normal;"><h2 class="wb-stl-heading2">PORQUE ES ATRACTIVO INVERTIR EN ESTE PROYECTO EN PANELES? </h2>
</div><div id="wb_element_instance45" class="wb_element" style=" line-height: normal;"><h2 class="wb-stl-heading2">Nuestra Visión</h2>
</div><div id="wb_element_instance46" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal">Ser la empresa líder y socialmente responsable a nivel estatal por prestar el servicio más completo de venta de paneles y calentadores solares, creando así conciencia del cuidado ambiental. </p>
</div><div id="wb_element_instance47" class="wb_element" style="width: 100%;">
			<?php
				global $show_comments;
				if (isset($show_comments) && $show_comments) {
					renderComments(2);
			?>
			<script type="text/javascript">
				$(function() {
					var block = $("#wb_element_instance47");
					var comments = block.children(".wb_comments").eq(0);
					var contentBlock = $("#wb_main");
					contentBlock.height(contentBlock.height() + comments.height());
				});
			</script>
			<?php
				} else {
			?>
			<script type="text/javascript">
				$(function() {
					$("#wb_element_instance47").hide();
				});
			</script>
			<?php
				}
			?>
			</div></div>
<div class="vbox wb_container" id="wb_footer" style="height: 174px;">
	
<div id="wb_element_instance31" class="wb_element" style=" line-height: normal;"><p class="wb-stl-footer">© 2016 <a href="http://ecot-center.esy.es">ecot-center.esy.es</a></p></div><div id="wb_element_instance32" class="wb_element"><div class="wb-stl-footer" style="white-space: nowrap;">Miembro de <i class="icon-wb-logo"></i><a href="http://zyro.com/examples/" target="_blank" title="Zyro - Website Builder">Zyro</a></div><script type="text/javascript">
				var _siteProBadge = _siteProBadge || [];
				_siteProBadge.push({hash: "c40c062b0c15d560008c8d7130537bf1", cont: "wb_element_instance32"});

				(function() {
					var script = document.createElement("script");
					var src = "http://zyro.com/examples/getjs/";
					script.type = "text/javascript";
					script.async = true;
					script.src = src.replace(/http.*:/, location.protocol);
					var s = document.getElementsByTagName("script")[0];
					s.parentNode.insertBefore(script, s);
				})();
				</script></div><div id="wb_element_instance33" class="wb_element"><a href="https://www.facebook.com/Ecot-Center-219009471801603/" target="1"><img alt="facebook" src="gallery_gen/15483c9714e92abb39ad0401f75ebbbc_40x40.png"></a></div><div id="wb_element_instance34" class="wb_element"><a href="https://twitter.com/EcotCenter" target="1"><img alt="twitter" src="gallery_gen/471b48acc334e90048e8de270c1df603_40x40.png"></a></div><div id="wb_element_instance48" class="wb_element" style="text-align: center; width: 100%;"><div class="wb_footer"></div><script type="text/javascript">
			$(function() {
				var footer = $(".wb_footer");
				var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
				if (!html) {
					footer.parent().remove();
					footer = $("#wb_footer");
					footer.height(94);
				}
			});
			</script></div></div><div class="wb_sbg"></div></div></body>
</html>
