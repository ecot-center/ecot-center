<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Inicio</title>
	<base href="{{base_url}}" />
			<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="description" content="" />
	<meta name="keywords" content="" />
		<meta name="generator" content="Zyro - Website Builder" />
	
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/main.js" type="text/javascript"></script>

	<link href="css/site.css?v=1.1.30" rel="stylesheet" type="text/css" />
	<link href="css/common.css?ts=1462483823" rel="stylesheet" type="text/css" />
	<link href="css/1.css?ts=1462483823" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="/gallery/wind_logo.png" type="image/png" />
	<script type="text/javascript">var currLang = '';</script>		
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>


<body>{{ga_code}}<div class="root"><div class="vbox wb_container" id="wb_header">
	
<div id="wb_element_instance0" class="wb_element"><a class="btn btn-default btn-collapser"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><ul class="hmenu"><li class="active"><a href="Inicio/" target="_self" title="Inicio">Inicio</a></li><li><a href="Acerca-de/" target="_self" title="Acerca de">Acerca de</a></li><li><a href="Como-Funciona/" target="_self" title="Como Funciona">Como Funciona</a></li><li><a href="Contactos/" target="_self" title="Contactos">Contactos</a></li></ul><script type="text/javascript"> (function() { var isOpen = false, elem = $('#wb_element_instance0'), btn = elem.children('.btn-collapser').eq(0); btn.on('click', function() { if (elem.hasClass('collapse-expanded')) { isOpen = false; elem.removeClass('collapse-expanded'); } else { isOpen = true; elem.addClass('collapse-expanded'); } }); })(); </script></div><div id="wb_element_instance1" class="wb_element" style=" line-height: normal;"><h4 class="wb-stl-pagetitle">Ecot - Center</h4>
</div><div id="wb_element_instance2" class="wb_element"><a href="Inicio/"><img alt="wind_logo" src="gallery_gen/0f3516698ba893423b36a442868afd23_90x90.png"></a></div></div>
<div class="vbox wb_container" id="wb_main">
	
<div id="wb_element_instance7" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1">Para tu mayor comodidad ahora desde tu dispositivo movil</h1>
</div><div id="wb_element_instance8" class="wb_element"><img alt="ios_android" src="gallery_gen/ca9c8c46a36e1b46938dd50d2378e16e_460x310.png"></div><div id="wb_element_instance9" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal"><span style="background-color: transparent;">    Para brindarles un mejor servicio les proporcionaremos una aplicacion para tu dispositivo con el cual podran cotizar nuestros productos en cualquier lugar con una conexion a internet.</span></p>

<p class="wb-stl-normal"> </p>

<p class="wb-stl-normal">    Por el momento pueden realizar sus cotizaciones en los numeros de contacto o por medio del formulario dentro de Contactactos, en la brevedad le haremos llegar su cotizacion.</p>
</div><div id="wb_element_instance10" class="wb_element" style=" line-height: normal;"><p style="text-align: justify;">Con capacidad para 5 personas (10 tubos con capacidad de 102 LTS en total)</p>

<p style="text-align: center;">desde:</p>

<p> </p>

<h1 class="wb-stl-heading1" style="text-align: center;">$ XXX.XXX</h1>
</div><div id="wb_element_instance11" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1" style="text-align: center;">Paneles Solares</h1>
</div><div id="wb_element_instance12" class="wb_element" style=" line-height: normal;"><h5 class="wb-stl-subtitle" style="text-align: center;"><span class="wb-stl-highlight">Favorable al medio ambiente</span></h5></div><div id="wb_element_instance13" class="wb_element" style=" line-height: normal;"><h5 class="wb-stl-subtitle" style="text-align: center;">Soluciones inteligentes</h5></div><div id="wb_element_instance14" class="wb_element"><div></div></div><div id="wb_element_instance15" class="wb_element"><div></div></div><div id="wb_element_instance16" class="wb_element"><div></div></div><div id="wb_element_instance17" class="wb_element"><div></div></div><div id="wb_element_instance18" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1" style="text-align: center;">Repuestos en general</h1>
</div><div id="wb_element_instance19" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1" style="text-align: center;">Calentadores Solares</h1>
</div><div id="wb_element_instance20" class="wb_element"><a class="wb_button" href=""><span>Cotizar</span></a></div><div id="wb_element_instance21" class="wb_element"><img alt="cutmypic (2)" src="gallery_gen/38990bd0590734cdba251e1185468247_160x160.png"></div><div id="wb_element_instance22" class="wb_element"><img alt="cutmypic" src="gallery_gen/5bf647eda66502a2c545192590f8805f_160x160.png"></div><div id="wb_element_instance23" class="wb_element"><img alt="cutmypic (1)" src="gallery_gen/0e05a598f997536b7164ed20ccf6a88c_160x160.png"></div><div id="wb_element_instance24" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal" style="text-align: justify;">Contamos con todo tipo de repuestos ya sea para calentadores  o panel solares, desde inversores de corriente, paneles fotovoltaicos, hasta tanques de acero inoxidable, tubos de vidrio y mucho mas... </p>

<p class="wb-stl-normal"> </p>

<p class="wb-stl-normal" style="text-align: center;"> </p>
</div><div id="wb_element_instance25" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal">Contamos con asesoria para crear tu paquete adaptado a tus necesidades.</p>

<p class="wb-stl-normal"> </p>

<p class="wb-stl-normal" style="text-align: center;">desde:</p>

<p class="wb-stl-normal" style="text-align: center;"> </p>

<h3 class="wb-stl-heading3" style="text-align: center;">Precio dependiendo del consumo requerido</h3>
</div><div id="wb_element_instance26" class="wb_element" style="width: 100%;">
			<?php
				global $show_comments;
				if (isset($show_comments) && $show_comments) {
					renderComments(1);
			?>
			<script type="text/javascript">
				$(function() {
					var block = $("#wb_element_instance26");
					var comments = block.children(".wb_comments").eq(0);
					var contentBlock = $("#wb_main");
					contentBlock.height(contentBlock.height() + comments.height());
				});
			</script>
			<?php
				} else {
			?>
			<script type="text/javascript">
				$(function() {
					$("#wb_element_instance26").hide();
				});
			</script>
			<?php
				}
			?>
			</div></div>
<div class="vbox wb_container" id="wb_footer" style="height: 174px;">
	
<div id="wb_element_instance3" class="wb_element" style=" line-height: normal;"><p class="wb-stl-footer">© 2016 <a href="http://ecot-center.esy.es">ecot-center.esy.es</a></p></div><div id="wb_element_instance4" class="wb_element"><div class="wb-stl-footer" style="white-space: nowrap;">Miembro de <i class="icon-wb-logo"></i><a href="http://zyro.com/examples/" target="_blank" title="Zyro - Website Builder">Zyro</a></div><script type="text/javascript">
				var _siteProBadge = _siteProBadge || [];
				_siteProBadge.push({hash: "c40c062b0c15d560008c8d7130537bf1", cont: "wb_element_instance4"});

				(function() {
					var script = document.createElement("script");
					var src = "http://zyro.com/examples/getjs/";
					script.type = "text/javascript";
					script.async = true;
					script.src = src.replace(/http.*:/, location.protocol);
					var s = document.getElementsByTagName("script")[0];
					s.parentNode.insertBefore(script, s);
				})();
				</script></div><div id="wb_element_instance5" class="wb_element"><a href="https://www.facebook.com/Ecot-Center-219009471801603/" target="1"><img alt="facebook" src="gallery_gen/15483c9714e92abb39ad0401f75ebbbc_40x40.png"></a></div><div id="wb_element_instance6" class="wb_element"><a href="https://twitter.com/EcotCenter" target="1"><img alt="twitter" src="gallery_gen/471b48acc334e90048e8de270c1df603_40x40.png"></a></div><div id="wb_element_instance27" class="wb_element" style="text-align: center; width: 100%;"><div class="wb_footer"></div><script type="text/javascript">
			$(function() {
				var footer = $(".wb_footer");
				var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
				if (!html) {
					footer.parent().remove();
					footer = $("#wb_footer");
					footer.height(94);
				}
			});
			</script></div></div><div class="wb_sbg"></div></div></body>
</html>
