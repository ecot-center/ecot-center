<?php
    // Replace with the real server API key from Google APIs
    $apiKey = "AIzaSyBigfQtSw7QJo_6LcuIlSYjwc06zn-3J7k";

    // Replace with the real client registration IDs
    $registrationIDs = array( "APA91bH7WCBzoZAyqzLyx62vu_r6J2IkZjx2Syet7XesSrbQoaI849f2oMmTmGC5jodBY11DzRtuEKSaUCBoYUhVlscuhB5touA6zdd4S65Y0eOaNd5JsXTvJrJVlEsnAFpGEPI0BdNy");

    // Message to be sent
    $message = "Your message e.g. the title of post";

    // Set POST variables
    $url = 'https://android.googleapis.com/gcm/send';

    $fields = array(
        'registration_ids' => $registrationIDs,
        'data' => array( "message" => $message,"payload" => array("android"=>array("alert"=>"prueba desde pagina php","sound"=>"default","vibrate"=>"true"))),

    );
    $headers = array(
        'Authorization: key=' . $apiKey,
        'Content-Type: application/json'
    );

    // Open connection
    $ch = curl_init();

    // Set the URL, number of POST vars, POST data
    curl_setopt( $ch, CURLOPT_URL, $url);
    curl_setopt( $ch, CURLOPT_POST, true);
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $fields));

    // Execute post
    $result = curl_exec($ch);

    // Close connection
    curl_close($ch);
    // print the result if you really need to print else neglate thi
    echo $result;
    //print_r($result);
    //var_dump($result);
?>